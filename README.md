# Pasos a seguir para configurar VM:

### 1. Configuración VM:

Nuestro archivo Vagrantfile esta configurado para realizar la instalación y configuración de la maquina virtual, por lo cual solo se debe utilizar `vagrant up` y una vez lista la VM, acceder a ella y realizar los siguientes comandos para tener el website funcionando:

```bash
cd /vagrant/sites/www.socialhub.local/
composer install
npm install && npm run dev
npm run dev
```

### 2. Configuración para DB

Modificar datos del .env con los siguientes:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=socialhub
DB_USERNAME=warrior
DB_PASSWORD=12345678
```

Una vez configurada la DB, deberemos usar `php artisan migrate` para generar las tablas en la DB.
