# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "debian/bullseye64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.33.11"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
  config.vm.synced_folder "sites/", "/home/vagrant/sites", owner: "www-data", group: "www-data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  config.vm.provision "shell", inline: <<-SHELL
    echo -e "\033[31;7m Cambiando Hostname \e[0m" && sudo hostnamectl set-hostname webserver
    echo -e "\033[31;7m Modificando archivo hosts \e[0m" && sudo sed -i 's/bullseye/webserver webserver.com/' /etc/hosts
    echo -e '\033[31;7m Apt-get Update \e[0m' && sudo apt-get update
    echo -e '\033[31;7m Apt-get install \e[0m' && sudo apt-get install -y git vim vim-nox curl apache2 php7.4 php7.4-bcmath php7.4-curl php7.4-json php7.4-mbstring php7.4-mysql php7.4-xml php7.4-zip mariadb-server
    sudo a2enmod vhost_alias rewrite
    echo -e '\033[31;7m Reiniciando apache2 \e[0m' && sudo systemctl restart apache2
    echo -e '\033[31;7m copiando www.socialhub.local.conf \e[0m' && sudo cp /vagrant/www.socialhub.local.conf /etc/apache2/sites-available 
    sudo a2ensite www.socialhub.local
    echo -e "\nServerName webserver.com" | sudo tee -a /etc/apache2/apache2.conf 
    echo -e '\033[31;7m Reiniciando apache2 \e[0m' && sudo systemctl reload apache2
    echo -e '\033[31;7m Instalando Composer \e[0m' && sh /vagrant/install_composer.sh
    sudo mkdir /opt/composer
    sudo mv composer.phar /opt/composer/
    sudo ln -s /opt/composer/composer.phar /usr/bin/composer
    echo -e '\033[31;7m Instalando NVM \e[0m' && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    source ~/.bashrc
    echo -e '\033[31;7m Instalando NodeJS \e[0m' && nvm install v16.14.2
    echo -e '\033[31;7m Creando DB \e[0m' && sudo mysql -e 'create database socialhub';
    echo -e '\033[31;7m Creando usuario en la DB \e[0m' && sudo mysql -e "create user warrior identified by '12345678'";
    echo -e '\033[31;7m Dando privilegios al usuario en la DB \e[0m' &&sudo mysql -e "grant all privileges on socialhub.* to warrior@'%'";
    sudo mysql -e "flush privileges";
    echo -e '\033[31;7m Comentando bind-address en DB config \e[0m' && sudo sed -i 's/bind-address            = 127.0.0.1/#bind-address            = 127.0.0.1/' /etc/mysql/mariadb.conf.d/50-server.cnf
    echo -e '\033[31;7m Reiniciando DB \e[0m' && sudo systemctl restart mysql
  SHELL
end
